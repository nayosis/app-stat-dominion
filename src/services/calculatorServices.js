
import partiesData from '../data/parties.json'
import joueursData from '../data/joueurs.json'




export const playerSort = (x, y) => {
    if (y.score === x.score) {
        return x.tour - y.tour
    }
    return y.score - x.score
}




export function getAllPartie() {
    return partiesData.map(parti => {
        const listjoueur = parti.joueurs.map(joueur => {
            const configPlayer = joueursData.find(innerJoueur => innerJoueur.pseudo === joueur.pseudo);

            return { ...joueur, ...configPlayer }
        })
        return { ...parti, joueurs: listjoueur }

    })

}

export function nombreVictoireAgainst(parties, joueur1, joueur2) {

    return parties.filter(partie => {
        return joueurGagnant(partie)?.player?.pseudo === joueur1.pseudo
    })
        .filter(partie => {
            return partie.joueurs.map(innerJoueur => innerJoueur.pseudo).includes(joueur2.pseudo)
        }).length

}

export function joueurGagnant(partie) {
    const joueurs = [...partie.joueurs]
    const orderedPlayer = joueurs.sort(playerSort);

    const nombreDeJoueurs = orderedPlayer.length;


    const theplayer = orderedPlayer.reduce((acc, val) => {
        if (acc.item === "none") {
            return { item: val }

        }
        if (acc.item === undefined) {
            return { item: undefined }
        }
        if (acc.item.tour === val.tour && acc.item.score === val.score) {
            return { item: undefined }
        }
        return acc;

    }, { item: "none" })

    return { player: theplayer.item, contre: nombreDeJoueurs.length - 1 }

}


export function scoreTotalByPlayer(playerName) {

    const partiOfPlayer = partiesData.filter(partie => {
        return partie.joueurs.find(joueur => joueur.pseudo === playerName) != undefined
    })

    const totalScore = partiOfPlayer.reduce((acc, partie) => {
        return acc + partie.joueurs.find(joueur => joueur.pseudo === playerName).score
    }, 0)

    const partieGagne = partiOfPlayer.filter(partie => {
        return joueurGagnant(partie)?.player?.pseudo === playerName
    })

    const nombreDeJoueurBatu = partieGagne.reduce((acc, item) => {
        return acc + item.joueurs.length - 1;
    }, 0)

    const totalGagne = partieGagne.length

    const matchCount = partiOfPlayer.length
    const moyenne = totalScore / matchCount



    return {
        "matchCount": matchCount,
        "totalGagne": totalGagne,
        "totalScore": totalScore,
        "avg": moyenne,
        "txReussite": ((totalGagne / matchCount) * 100).toFixed(),
        "joueurBatue": nombreDeJoueurBatu
    }



}