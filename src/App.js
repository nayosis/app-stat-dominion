
import { Container } from 'semantic-ui-react'
import ResultatPage from './pages/resultatPage';
import './file.scss'


function App() {
  return (
    <div className='container'>
      <ResultatPage></ResultatPage>
    </div>
  );
}

export default App;
