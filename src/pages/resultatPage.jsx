import { Header, Card, Statistic, Grid, Image, Label, Table, Icon, Segment } from 'semantic-ui-react'
import Avatar from '../components/avatar'


import joueurs from '../data/joueurs.json'
import { getAllPartie, joueurGagnant, playerSort, scoreTotalByPlayer } from '../services/calculatorServices'
import PartiePointRepartition from '../components/graph/PartiePointRepartition'
import MyResponsiveChord from '../components/graph/Test'



const ScorePlayer = ({ score }) => {


    return (<Statistic.Group horizontal size='mini'>
        <Statistic>
            <Statistic.Value>{score.avg.toFixed(2)}</Statistic.Value>
            <Statistic.Label>Moyenne</Statistic.Label>
        </Statistic>
        <Statistic>
            <Statistic.Value>{score.totalScore}</Statistic.Value>
            <Statistic.Label>Score Total</Statistic.Label>
        </Statistic>
        <Statistic>
            <Statistic.Value>{score.matchCount}</Statistic.Value>
            <Statistic.Label>Matchs</Statistic.Label>
        </Statistic>
        <Statistic>
            <Statistic.Value>{score.totalGagne} (<Icon name='male' />{score.joueurBatue})</Statistic.Value>
            <Statistic.Label>Victoires</Statistic.Label>
        </Statistic>

        <Statistic>
            <Statistic.Value>{score.txReussite} %</Statistic.Value>
            <Statistic.Label>Reussite</Statistic.Label>
        </Statistic>


    </Statistic.Group>
    )
}


const ResultatPage = () => {



    return <>

        <h2>Liste des joueurs</h2>



        <div className='board'>
            <div style={{ "width": "40%", "height": "30vh" }}>

                <MyResponsiveChord joueurs={joueurs} parties={getAllPartie()} />


            </div>
            <div className='players'>

                {joueurs.map(joueur => {
                    return (<div className='player'>
                        <Segment >

                            <Header><Icon name={"male"}></Icon>{joueur.pseudo}</Header>
                            <ScorePlayer score={scoreTotalByPlayer(joueur.pseudo)} />

                        </Segment>

                    </div>)
                })}

            </div>

        </div>




        <h2>Liste des parties</h2>

        <table className='table bordered' >
            <tr className='table-header'>

                <td >Gagnant</td>
                <td >date</td>
                <td > Data</td>
                <td colspan="4"></td>

            </tr>
            {getAllPartie().map(partie => {

                return (<tr>

                    <td collapsing > <Avatar joueur={joueurGagnant(partie).player} /> </td>
                    <td collapsing >{partie.date}</td>
                    <td  ><div style={{ "height": "15vh" }}><PartiePointRepartition partie={partie} /></div></td>




                    {partie.joueurs.sort(playerSort).map((joueur, index) => {
                        return (<>
                            <td>
                                <Avatar joueur={joueur} />
                                <span>
                                    ({joueur.score} pts |  {joueur.tour} tours)
                                </span>
                            </td>
                        </>)
                    })}
                    {Array.from({ length: 4 - partie.joueurs.length }, (_, index) => {
                        return <td />;
                    })}

                </tr>)
            })}

        </table>




    </>
}

export default ResultatPage


