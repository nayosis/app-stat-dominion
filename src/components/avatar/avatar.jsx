import './avatar.css'

const Avatar = ({ joueur }) => {


    return <div className='vgo-avatar' style={{ "background-color": joueur.color }} >
        {joueur.pseudo}
    </div>
}


export default Avatar