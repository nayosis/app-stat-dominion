import { ResponsiveChord } from '@nivo/chord'
import { nombreVictoireAgainst } from '../../services/calculatorServices';

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.





const MyResponsiveChord = ({ parties, joueurs /* see data tab */ }) => {


    const size = joueurs.length;
    const data = joueurs.map((joueur1, index1) => {


        return joueurs.map((joueur2, index2) => {
            if (index1 === index2) {
                return 0
            }
            return nombreVictoireAgainst(parties, joueur1, joueur2)

        })
    })

    const datfa = [
        [
            0,
            0,
            2
        ],
        [
            1,
            0,
            3
        ],
        [
            2,
            3,
            0
        ]
    ]

    return (
        <ResponsiveChord
            data={data}
            keys={['ga12heg', 'flo', 'nayosis']}
            margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
            colors={(datum) => { console.log("vincent ", datum); return joueurs[datum.index].color }}

            padAngle={0.3}
            innerRadiusRatio={0.8}
            innerRadiusOffset={0.02}
            inactiveArcOpacity={0.25}

            activeRibbonOpacity={0.75}
            inactiveRibbonOpacity={0.25}
            ribbonBorderColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        0.6
                    ]
                ]
            }}
            labelRotation={0}
            labelTextColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        1
                    ]
                ]
            }}
            //colors={{ scheme: 'nivo' }}
            motionConfig="stiff"
            legends={[
                {
                    anchor: 'bottom',
                    direction: 'row',
                    justify: false,
                    translateX: 0,
                    translateY: 70,
                    itemWidth: 80,
                    itemHeight: 14,
                    itemsSpacing: 0,
                    itemTextColor: '#999',
                    itemDirection: 'left-to-right',
                    symbolSize: 12,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000'
                            }
                        }
                    ]
                }
            ]}
        />
    )
}

export default MyResponsiveChord