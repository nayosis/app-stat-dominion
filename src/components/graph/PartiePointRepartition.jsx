// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/pie
import { ResponsivePie } from '@nivo/pie'

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
const PartiePointRepartition = ({ partie /* see data tab */ }) => {

    const data = partie?.joueurs?.map(joueur => {
        return {
            "id": joueur.pseudo,
            "label": joueur.pseudo,
            "value": joueur.score,
            "color": joueur.color
        }
    })

    return (

        <ResponsivePie
            data={data}
            colors={{ datum: 'data.color' }}
            margin={{ top: 5, right: 5, bottom: 5, left: 5 }}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={3}
            activeOuterRadiusOffset={8}
            borderWidth={1}
            borderColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        0.2
                    ]
                ]
            }}
            enableArcLinkLabels={false}
            arcLinkLabelsSkipAngle={10}
            arcLinkLabelsTextColor="#333333"
            arcLinkLabelsThickness={2}
            arcLinkLabelsColor={{ from: 'color' }}
            arcLabelsSkipAngle={10}



        />)
}



export default PartiePointRepartition